import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.formRuta.setValue('');
  }

  formRuta = new FormControl({

  });

  goURL() {
    if(this.formRuta.value != ''){
      window.open(this.formRuta.value, '_blank');
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Redireccionamiento Satisfactorio',
        showConfirmButton: true
      })
      this.formRuta.setValue('');
    }else{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Asegúrese de copiar una dirección URL',
        showConfirmButton: false,
        timer: 1500
      })
    }

  }
}
